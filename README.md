# Game

## Get started

In project root do:

    npm install
    grunt build
    grunt serve

This will run a [server on port 8888](http://localhost:8888).


## Prerequisites

Need to have _nodejs_, _npm_ and _grunt-cli_ installed.


## About

The goal is an application which serves multiple game rooms where players
can play a memory game. The first two players in the room start playing
to each other. Other visitors in the room can follow the play.

It's an experiment using websockets and backbone together.


_This is work in progress. So... no actual games to be played right now._