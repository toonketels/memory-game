var app = require('express')()
  , express = require('express')
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , game = require('./apps/game')
  , room = require('./apps/room');


app.use(express.static(__dirname + '/../www'));

server.listen(8888);

io.sockets.on('connection', function (socket) {

  room.handle(socket, io);
  game.handle(socket, io);

});