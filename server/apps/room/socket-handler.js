var db = require('./collection').db
  , moniker = require('moniker')
  , _ = require('underscore');



var handle = function(socket, io) {

  /**
   * rooms:read
   *
   * Called when we .fetch() our collection
   * in the client-side router
   */

  socket.on('rooms:read', function (data, callback) {
      var items = _.map(db, function(room) { return room.attributes; });
      callback(null, items);
  });


  /**
   * room:read
   *
   * Called when we .fetch() a room model.
   */
  socket.on('room:read', function (data, callback) {
      // @todo: check if we get an item back.
      var room = _.findWhere(db, {id: +data.id});
      var json = room.attributes;
      callback(null, json);
    });


  /**
   * room:create
   *
   * Called when we .save() our new room
   *
   * We listen on model namespace, but emit
   * on the collection namespace so the collection
   * updates itself.
   */
  socket.on('room:create', function (data, callback) {
    var id = _.max(db, function(room) { return room.id; }).id + 1
      , name = moniker.choose()      // override the name, pick something unique
      , json = {name: name, id: id}
      , room = {id: id, attributes: json, totalMembers: 0, name: name};

    // Add to db
    db.push(room);

    socket.emit('rooms:create', json);
    socket.broadcast.emit('rooms:create', json);
    callback(null, json);
  });


  /**
   * subscribe
   *
   * A user has entered the room, He can:
   *  1. wait for second player
   *  2. be the second and start playing
   *  3. watch the game
   *
   * @todo: unsubsribing from room
   */ 
  socket.on('subscribe', function(data) { 
    socket.join(data.room);

    // Add user to room
    var room = _.findWhere(db, {name: data.room});
    room.totalMembers = io.sockets.clients(data.room).length;

    io.sockets.in(data.room).emit('count', {totalMembers: room.totalMembers});

    // @todo emit custom events depending on number of
    // members in the room
    switch (room.members) {
      case 1:
        break;
    }

  });


  /**
   * Unsubsubscribe from room.
   */
  socket.on('unsubscribe', function(data) { 
    socket.leave(data.room);

    var room = _.findWhere(db, {name: data.room});
    room.totalMembers = io.sockets.clients(data.room).length;

    io.sockets.in(data.room).emit('count', {totalMembers: room.totalMembers});
  });
}

module.exports.handle = handle;