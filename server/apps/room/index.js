/**
 * RoomApp.
 */
var socketHandler = require('./socket-handler');

module.exports.handle = socketHandler.handle;