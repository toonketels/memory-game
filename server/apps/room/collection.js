/**
 * Starting rooms.
 */
module.exports.db = [
  {
    id: 1,
    totalMembers: 0,
    name: 'solid-umbrella',
    attributes: {
      name: 'solid-umbrella',
      id: 1
    }
  },
  {
    id: 2,
    totalMembers: 0,
    name: 'plant-bell',
    attributes: {
      name: 'plant-bell',
      id: 2
    }
  }
];