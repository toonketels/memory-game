/**
 * Game.
 *
 * Responsible for fetching cards and starting a new
 * game.
 */
var socketHandler = require('./socket-handler.js');

module.exports.handle = socketHandler.handle;