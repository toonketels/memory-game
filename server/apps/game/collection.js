/**
 * Card collection.
 */
var _ =  require('underscore');

var cardSource = [
  {
    id: 1,
    authorName: 'Garrett Scaffani',
    authorUrl: "http://dribbble.com/turbosaurusrex",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/174489/screenshots/1131814/screen_shot_2013-06-26_at_9.57.51_am_1x.png",
    imgUrlInfo: "http://dribbble.com/shots/1131814-Another-Yeti-Logo",
    imgTitle: 'Another Yeti Logo',
    imgFile: "1131814-Another-Yeti-Logo.png"
  },
  {
    id: 2,
    authorName: 'Jeff Hilnbrand',
    authorUrl: "http://dribbble.com/jhilmd",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/232416/screenshots/1129250/hyp-dr_1x.png",
    imgUrlInfo: "http://dribbble.com/shots/1129250-Sight-1",
    imgTitle: 'Sight 1',
    imgFile: "1129250-Sight-1.png"
  },
  {
    id: 3,
    authorName: 'Nick Pring',
    authorUrl: "http://dribbble.com/nickpring",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/33056/screenshots/1126829/nickpring_dribbble_star1_1x.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1126829-Star-Logo-1",
    imgTitle: 'Star Logo 1',
    imgFile: "1126829-Star-Logo-1.png"
  },
  {
    id: 4,
    authorName: 'Matthew Reilly',
    authorUrl: "http://dribbble.com/Lickmystyle",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/127815/screenshots/1123922/tir-dribbble2_1x.png",
    imgUrlInfo: "http://dribbble.com/shots/1123922-Triangulation-Mosaic-Pattern-2-Demo",
    imgTitle: 'Triangulation Mosaic Pattern 2 Demo',
    imgFile: "1123922-Triangulation-Mosaic-Pattern-2-Demo.png"
  },
  {
    id: 5,
    authorName: 'luke dupont',
    authorUrl: "http://dribbble.com/lukedupont",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/14606/screenshots/1122542/injet_1x.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1122542-Injet-branding",
    imgTitle: 'Injet branding',
    imgFile: "1122542-Injet-branding.png"
  },
  {
    id: 6,
    authorName: 'Mood Design Studio',
    authorUrl: "http://dribbble.com/MoodDesignStudio",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/14412/screenshots/1118124/cloud.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1118124-Cloud",
    imgTitle: 'Cloud',
    imgFile: "1118124-Cloud.png"
  },
  {
    id: 7,
    authorName: 'Shota Mickaia',
    authorUrl: "http://dribbble.com/shotamickaia",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/176097/screenshots/1116534/chefcook.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1116534-ChefCook",
    imgTitle: 'ChefCook',
    imgFile: "1116534-ChefCook.png"
  },
  {
    id: 8,
    authorName: 'Paul Armstrong',
    authorUrl: "http://dribbble.com/wiseacre",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/992/screenshots/1113764/dribbble_helvetica-no_1x.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1113764-Helvetica-No",
    imgTitle: 'Helvetica No',
    imgFile: "1113764-Helvetica-No.png"
  },
  {
    id: 9,
    authorName: 'Pablo Alvarez Vinagre',
    authorUrl: "http://dribbble.com/pabloandco",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/311146/screenshots/1109738/colorless.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1109738-Colourless",
    imgTitle: 'Colourless',
    imgFile: "1109738-Colourless.png"
  },
  {
    id: 10,
    authorName: 'Vadim Prusakov',
    authorUrl: "http://dribbble.com/voodooslab",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/200979/screenshots/1108694/icon.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1108694-Icon",
    imgTitle: 'Icon',
    imgFile: "1108694-Icon.png"
  },
  {
    id: 11,
    authorName: 'Joseph Wells',
    authorUrl: "http://dribbble.com/josephw",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/335035/screenshots/1107525/cougar_wip.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1107525-Cougar-Wip",
    imgTitle: 'Cougar Wip',
    imgFile: "1107525-Cougar-Wip.png"
  },
  {
    id: 12,
    authorName: 'Muhammad Ali Effendy',
    authorUrl: "http://dribbble.com/effendy",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/5043/screenshots/1105271/rapidoware-cloud-dribbble_1x.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1105271-Cloud-Logo-Proposal",
    imgTitle: 'Cloud Logo ProposalCloud Logo Proposal',
    imgFile: "1105271-Cloud-Logo-Proposal.png"
  },
  {
    id: 13,
    authorName: 'Ehein',
    authorUrl: "http://dribbble.com/Ehein",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/4698/screenshots/1104446/untitled-1.png",
    imgUrlInfo: "http://dribbble.com/shots/1104446-Favorite-Color",
    imgTitle: 'Favorite Color',
    imgFile: "1104446-Favorite-Color.png"
  },
  {
    id: 14,
    authorName: 'sunpea',
    authorUrl: "http://dribbble.com/sunpea",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/10463/screenshots/1106639/julab_1x.png",
    imgUrlInfo: "http://dribbble.com/shots/1106639-Julabs",
    imgTitle: 'Julabs',
    imgFile: "1106639-Julabs.png"
  },
  {
    id: 15,
    authorName: 'Scott Baker',
    authorUrl: "http://dribbble.com/sdbaker",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/239963/screenshots/1136268/sb-logo-2x_1x.jpg",
    imgUrlInfo: "http://dribbble.com/shots/1136268-SB-Monogram",
    imgTitle: 'SB Monogram',
    imgFile: "1136268-SB-Monogram.png"
  },
  {
    id: 16,
    authorName: 'Jonathan Hasson',
    authorUrl: "http://dribbble.com/hasson",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/90627/screenshots/1136151/40-clothingbrand.png",
    imgUrlInfo: "http://dribbble.com/shots/1136151-Clothing-Brand-Logo",
    imgTitle: 'Clothing Brand Logo',
    imgFile: "1136151-Clothing-Brand-Logo.png"
  },
  {
    id: 17,
    authorName: 'alex slater',
    authorUrl: "http://dribbble.com/natako",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/45642/screenshots/1103342/mtv-1week_1x.png",
    imgUrlInfo: "http://dribbble.com/shots/1103342-Infographical",
    imgTitle: 'Infographical',
    imgFile: "1103342-Infographical.png"
  },
  {
    id: 18,
    authorName: 'Jonathan Hasson',
    authorUrl: "http://dribbble.com/hasson",
    imgUrlRemote: "http://dribbble.s3.amazonaws.com/users/90627/screenshots/1136156/41-tritri3.png",
    imgUrlInfo: "http://dribbble.com/shots/1136156-Impossible-Shape-Logo",
    imgTitle: 'Impossible Shape Logo',
    imgFile: "1136156-Impossible-Shape-Logo.png"
  }
];


var getNewDeck = function() {

  var id = cardSource.length + 1
    , cards = [];

  _.each(cardSource, function(source) {
    var card = _.clone(source);
    var sibling = _.clone(card);
    sibling.id = id;
    id ++;
    card.sibling = sibling.id;
    sibling.sibling = card.id;
    cards.push(card);
    cards.push(sibling);
  });

  cards = _.shuffle(cards);
  _.each(cards, function(card, index) {
    card.order = index;
  })

  return cards;
};

module.exports.getNewDeck = getNewDeck;

