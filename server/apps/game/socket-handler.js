/**
 * SocketHandler.
 *
 * Responsible for firing/handling events on the socket,
 * sort like a route handler in regular app.
 */
var collection = require('./collection')
  , db = require('../room/collection').db
  , _ = require('underscore');


var handle = function(socket, io) {

  /**
   * cards:read
   *
   * Called when we .fetch() our collection
   * in the client-side router
   */

  socket.on('cards:read', function (data, callback) {

    // find out which room they are in
    // check if we already have a deck for that room, if not create
    // return the deck
    var roomName = data.room;
    var room = _.findWhere(db, {name: data.room});
    if (!room.game) {
      room.game = collection.getNewDeck();
    }
  
    var items = room.game;
    callback(null, items);
  });


  /**
   * card:update
   *
   * called when we .save() our model
   */
  socket.on('card:update', function (data, callback) {

    // get our room collection
    // update the model
    // broadcast to all in the room
    var roomName = data.data.room;
    var room = _.findWhere(db, {name: roomName});
    var game = room.game;

    // Remove the room from model
    delete data.data;
    var clientCard = data;
    var serverCard, i, length;
    
    // Update our server side representation of the card
    for (i = 0, length = game.length; i < length; i++) {
      if (game[i].id === clientCard.id) {
        game[i] = serverCard = clientCard;
        break; 
      }
    }

    // Only emit to our room
    // socket.emit('card/' + data.id + ':update', serverCard);
    io.sockets.in(roomName).emit('card/' + data.id + ':update', serverCard);
    callback(null, serverCard);
  });

}

module.exports.handle = handle;