require.config({
  urlArgs: "bust=" + (new Date()).getTime(),
  deps: ['main'],
  baseUrl: './js/app',
  config: {
    i18n: {
      locale: 'nl-be'
    }
  },
  paths: {
    'jquery': '../vendor/jquery-1.9.1',
    'backbone': '../vendor/backbone',
    'json2': '../vendor/json2',
    'underscore': '../vendor/underscore',
    'backbone.babysitter': '../vendor/backbone.babysitter',
    'backbone.paginator': '../vendor/backbone.paginator',
    'backbone.wreqr': '../vendor/backbone.wreqr',
    'backbone.queryparams': '../vendor/backbone.queryparams',
    'backbone.iobind': '../vendor/backbone.iobind',
    'backbone.iosync': '../vendor/backbone.iosync',
    'backbone-relational': '../vendor/backbone-relational',
    'marionette': '../vendor/backbone.marionette',
    'text': '../vendor/text',
    'tpl': '../vendor/tpl',
    'i18n': '../vendor/i18n'
  },
  // Sets the configuration for your third party scripts
  // that are not AMD compatible
  shim: {
    'underscore': {
      exports: "_"
    },
    'backbone': {
      deps: ['jquery', 'json2', 'underscore'],
      exports: "Backbone"
    },
    'backbone.paginator': ['backbone', 'underscore', 'jquery'],
    'backbone.iobind': ['backbone'],
    'backbone.iosync': ['backbone'],
    'backbone.queryparams': ['underscore', 'backbone']
  }
});