// Require config used to go here
require(
  [
    'app',
    'backbone',
    'backbone.iosync',
    'backbone.iobind'
  ],

  function( app ){

    "use strict";

    app.start();
 

});