/**
 * MainApp.
 *
 * General AppWide stuff.
 */
define(
  [
    'marionette',
    'utils/state',
    'utils/socket'
  ],
  function( Marionette, state, socket ) {

    'use strict';

    var MainApp = Marionette.Controller.extend({
      
      initialize: function() {
        this.listenTo(state, 'change:room', this.forceSingleRoom, this);
      },

      forceSingleRoom: function(state, value) {
        if (state._previousAttributes.room) {
          socket.emit('unsubscribe', {room: state._previousAttributes.room});
        }
      }

    });

    return MainApp;

});