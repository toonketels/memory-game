/**
 * CardsListItemView.
 *
 * Displays a signle card in the cards list.
 */
define(
  [
    'marionette',
    'tpl!../templates/cards-list-item-view.tpl.html'
  ],
  function( Marionette, tpl, socket ) {

    'use strict';

    var CardsListItemView = Marionette.ItemView.extend({

      template: tpl,

      tagName: 'div',

      className: 'span2 card',

      events: {
        'click': 'toggleSelect'
      },

      modelEvents: {
        'change:selected': 'render'
      },

      templateHelpers: {
        getClass: function() {
          return this.selected ? 'selected' : 'deselected';
        }
      },

      toggleSelect: function() {
        if (this.model.get('selected')) {
          this.model.set('selected', false);
        } else {
          this.model.set('selected', true);
        }
      }

    });

    return CardsListItemView;

});