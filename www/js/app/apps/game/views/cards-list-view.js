/**
 * CardsListView.
 *
 * Displays a deck of cards.
 */
define(
  [
    'marionette',
    './cards-list-item-view'
  ],
  function( Marionette, CardsListItemView ) {

    'use strict';

    var CardsListItemView = Marionette.CollectionView.extend({

      itemView: CardsListItemView,

      tagName: 'div',

      className: 'row'

    });


    return CardsListItemView;

});