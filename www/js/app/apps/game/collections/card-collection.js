/**
 * CardCollection.
 *
 * A collection of cards.
 */
define(
  [
    'backbone',
    'utils/socket',
    '../models/card-model'
  ],
  function( Backbone, socket, CardModel ) {

    "use strict";

    var CardCollection = Backbone.Collection.extend({

      url: 'cards',

      socket: socket,

      model: CardModel

    });


    return CardCollection;

});