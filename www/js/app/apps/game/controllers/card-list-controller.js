/**
 * CardListController.
 *
 * Allows a list of cards to be displayed.
 */
define(
  [
    'marionette',
    '../collections/card-collection',
    '../views/cards-list-view',
    'utils/vent',
    'utils/state'
  ], 
  function( Marionette, CardCollection, CardsListView, vent, state ) {

    'use strict';

    var CardListController = Marionette.Controller.extend({

      displayCards: function() {
        var cards = new CardCollection();
        cards.fetch({data: {room: state.get('room')}});

        this.listenTo(cards, 'change:selected', this.saveSelectedState, this);

        var view = new CardsListView({collection: cards});
        vent.trigger('game:display:cards', {view: view});
      },

      // @todo: fix event from firing multiple times
      // we have some memory leaks in the sence
      // that the event will be called multiple times
      saveSelectedState: function(card, value) {
        card.save({data: {room: state.get('room')}});
      }

    });

    return CardListController;

});