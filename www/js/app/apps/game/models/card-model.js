/**
 * CardModel
 *
 * Contains the data for a single card.
 * Each card has it's twin card which the user should find.
 */
define(
  [
    'backbone',
    'utils/socket'
  ], 
  function( Backbone, socket ) {

    'use strict';

    var CardModel = Backbone.Model.extend({

      urlRoot: 'card',

      socket: socket,

      initialize: function() {
        this.ioBind('update', this.serverUpdate, this);
      },

      serverUpdate: function(data) {
        // @todo: check how to only send the items we want to change
        _.each(data, function(value, key) {
          this.set(key, value);
        }, this);
      }

    });


    return CardModel;

});