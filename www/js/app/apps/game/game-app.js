/**
 * GameApp.
 *
 * A game to play in a room.
 */
define(
  [
    'marionette',
    'utils/vent',
    'utils/socket',
    './Controllers/card-list-controller'
  ],
  function( Marionette, vent, socket, CardListController ) {

    'use strict';

    var GameApp = Marionette.Controller.extend({

      initialize: function(){
        this.initEventListening();
        this.cardListController = new CardListController();
      },

      initEventListening: function() {
        this.listenTo(vent, 'room:showed:detail', this.initGame, this);
      },

      /**
       * Actualy start a game
       */
      initGame: function(options) {
        var room = options.model;

        // Stat a game
        this.cardListController.displayCards();



      }

    });

    return GameApp;

});