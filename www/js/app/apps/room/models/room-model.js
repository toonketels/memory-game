/**
 * RoomModel
 *
 * A model representing a game room.
 */
define(
  [
    'backbone',
    'utils/socket',
  ],
  function( Backbone, socket ) {

    "use strict";

    var ModelRoom = Backbone.Model.extend({
  
      urlRoot: 'room',

      socket: socket

    });

    return ModelRoom;

});