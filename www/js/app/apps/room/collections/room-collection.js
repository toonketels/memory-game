/**
 * RoomCollection
 *
 * A collection of rooms.
 */
define(
  [
    'backbone',
    'utils/socket',
    '../models/room-model'
  ],
  function( Backbone, socket, RoomModel ) {

    "use strict";

    var RoomCollection = Backbone.Collection.extend({

      initialize: function() {
        this.initEventslistening();
        this.initServerEventsListening();
      },

      initEventslistening: function() {
      },

      initServerEventsListening: function() {
        this.ioBind('create', this.serverCreate, this);
      },

      model: RoomModel,

      socket: socket,

      url: 'rooms',

      // Create new rooms...
      serverCreate: function(data) {
        // make sure no duplicates, just in case
        var exists = this.get(data.id);
        if (!exists) {
          this.add(data);
        } else {
          data.fromServer = true;
          exists.set(data);
        }
      },

      onClose: function() {
        
      }

    });

    return RoomCollection;

});