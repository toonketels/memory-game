/**
 * RoomItemView.
 *
 * Displays a single room of the collection.
 */
define(
  [
    'marionette',
    'tpl!../templates/room-item-view.tpl.html',
    'utils/vent'
  ], 
  function( Marionette, roomItemViewTpl, vent ) {

    "use strict";

    var RoomItemView = Marionette.ItemView.extend({

      className: 'span6 room',

      template: roomItemViewTpl,
      
      events: {
        'click': 'goToDetail'
      },

      goToDetail: function() {
        vent.trigger('room:show:detail', {model: this.model, id: this.model.id});
      }

    });

    return RoomItemView;

});