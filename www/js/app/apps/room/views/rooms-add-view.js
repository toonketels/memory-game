/**
 * RoomsAddView
 *
 * Displays a button to add a view to the collection.
 */
define(
  [
    'marionette',
    'tpl!../templates/rooms-add-view.tpl.html',
    '../models/room-model'
  ],
  function( Marionette, tpl, RoomModel ) {

    'use strict';

    var RoomsAddView = Marionette.ItemView.extend({

      template: tpl,

      events: {
        'click': 'addRoom'
      },

      addRoom: function() {
        var model = new RoomModel({name: 'The new room repeated'});
        model.save();
      }

    });

    return RoomsAddView;

});