/**
 * Room Layout.
 *
 * Different regions for the room page.
 */
define(
  [
    'marionette',
    'tpl!../templates/room-layout.tpl.html'
  ], 
  function( Marionette, roomLayout ) {

    'use strict';

    var RoomLayout = Marionette.Layout.extend({

      template: roomLayout,

      regions: {
        topRegion: '.top',
        middleRegion: '.middle',
        bottomRegion: '.bottom'
      }

    });

    return RoomLayout;

});