/**
 * RoomDetailView.
 *
 * Displays one room as the detail.
 */
define(
  [
    'marionette',
    'tpl!../templates/room-detail-view.tpl.html'
  ],
  function( Marionette, roomDetailViewTpl ) {

    'use strict';

    var RoomDetailView = Marionette.ItemView.extend({

      template: roomDetailViewTpl,

      initialize: function() {
        this.listenTo(this.model, 'sync', this.render, this);
      }

    });

    return RoomDetailView;

});