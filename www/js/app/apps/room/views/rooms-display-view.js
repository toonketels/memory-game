/**
 * RoomsDisplayView
 *
 * Displays a list of all current rooms.
 */
define(
  [
    'marionette',
    './room-item-view'
  ],
  function( Marionette, RoomItemView ) {

    'use strict';

    var RoomsDisplayView = Marionette.CollectionView.extend({

      itemView: RoomItemView,

      tagName: 'div',

      className: 'row'

    });

    return RoomsDisplayView;

});