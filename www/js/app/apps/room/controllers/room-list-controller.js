/**
 * RoomListController
 *
 * Controls the display of a list of rooms.
 */
define(
  [
    'marionette',
    '../collections/room-collection',
    '../views/room-layout',
    '../views/rooms-display-view',
    '../views/rooms-add-view',
    'utils/vent'
  ],
  function( Marionette, RoomCollection, RoomLayout, RoomsDisplayView, RoomsAddView, vent  ) {

    'use strict';

    var RoomListController = Marionette.Controller.extend({

      initialize: function() {
        this.initEventListening();
      },

      initEventListening: function() {
        this.listenTo(vent, 'room:show:list', this.displayList, this);
      },

      displayList: function() {
        var roomCollection = this.getRoomCollection();
        var layout = this.displayLayout();
        var view = this.displayRoomsInRegion(roomCollection, layout.middleRegion);
        var addRoomView = this.dispayAddRoomViewInRegion(layout.bottomRegion);
      },

      // Only fetch the collection once, maybe we should
      // always fetch the collection for it to be up to date?
      getRoomCollection: function() {
        if (!this.roomCollection) {
          this.roomCollection =  this.fetchRooms();
        }
        return this.roomCollection;
      },

      fetchRooms: function() {
        var roomCollection = new RoomCollection();
        roomCollection.fetch();
        return roomCollection;        
      },

      displayLayout: function() {
        var layout = new RoomLayout();
        vent.trigger('room:display:all', {view: layout});
        return layout;
      },

      displayRoomsInRegion: function(rooms, region) {
        var view = new RoomsDisplayView({collection: rooms});
        region.show(view);
        return view;
      },

      dispayAddRoomViewInRegion: function(region) {
        var view = new RoomsAddView();
        region.show(view);
        return view;
      }

    });

    return RoomListController;

});