/**
 * RoomListController
 *
 * Controls the display of a single of game room.
 */
define(
  [
    'marionette',
    '../collections/room-collection',
    '../views/room-layout',
    '../views/room-detail-view',
    '../models/room-model',
    'utils/vent',
    'utils/socket',
    'utils/state'
  ],
  function( Marionette, RoomCollection, RoomLayout, RoomDetailView, RoomModel, vent, socket, state  ) {

    'use strict';

    var RoomViewController = Marionette.Controller.extend({

      initialize: function() {
        this.initEventListening();
      },

      initEventListening: function() {
        this.listenTo(vent, 'room:show:detail', this.displayDetail, this);
        this.listenTo(vent, 'game:display:cards', this.displayCards, this);
      },

      displayCards: function(options) {
        if (this.layout) {
          this.layout.middleRegion.show(options.view);
        }
      },

      displayDetail: function(options) {
        var model;

        if (options.model) {
          model = this.displayDetailForModel(options.model);
        } else {
          model = this.displayDetailForId(options.id);
        }

        // Subscribe to room, as soon as we got the room name
        if (model.has('name')) {
          this.subcribeToRoom(model.get('name'));
          state.set('room', model.get('name'));
        } else {
          model.on('sync', function() { 
            this.subcribeToRoom(model.get('name'));
            state.set('room', model.get('name'));
          }, this);
        }

        // @todo: change to proper event.
        vent.trigger('room:showed:detail', {model: model});
      },

      displayDetailForModel: function(model) {
        var layout = this.displayLayout();
        this.displayRoomInRegion(model, layout.topRegion);

        return model;
      },

      displayDetailForId: function(id) {
        var model = new RoomModel({id: id});
        model.fetch();
        var layout = this.displayLayout();
        this.displayRoomInRegion(model, layout.topRegion);
        
        return model;      
      },

      displayLayout: function() {
        var layout = new RoomLayout();
        vent.trigger('room:display:all', {view: layout});
        this.layout = layout;
        return layout;
      },

      displayRoomInRegion: function(room, region) {
        var view = new RoomDetailView({model: room});
        region.show(view);
      },

      subcribeToRoom: function(room) {
        socket.emit('subscribe', {room: room});
      }

    });

    return RoomViewController;

});