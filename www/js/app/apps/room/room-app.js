/**
 * RoomApp
 *
 * This is the main entry point for the rooms application.
 *
 * Rooms are the different places games can be played.
 *
 * This application is concerned about displaying/adding rooms.
 */
define(
  [
    'marionette',
    './controllers/room-list-controller',
    './controllers/room-view-controller',
    './routes/room-router',
    'utils/vent'
  ],
  function( Marionette, RoomListController, RoomViewController, RoomRouter, vent ) {

    "use strict";

    var RoomApp = Marionette.Controller.extend({

      initialize: function() {
        var roomListController = new RoomListController();
        var roomViewController = new RoomViewController();
        var router = new RoomRouter();
      }

    });

    return RoomApp;

});