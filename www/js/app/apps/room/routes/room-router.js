/**
 * RoomRoutes.
 *
 * The different routes for the room app.
 */
define(
  [
    'marionette',
    'utils/vent'
  ],
  function( Marionette, vent ) {

    'use strict';

    var RoomListRouter = Marionette.AppRouter.extend({

      initialize: function() {
        this.initEventListening();
      },

      initEventListening: function() {
        this.listenTo(vent, 'room:show:detail', this.updateUrlForDetail, this);
        this.listenTo(vent, 'room:show:list', this.updateUrlForList, this);
      },

      routes: {
        'rooms/:id': 'displayDetail',
        'rooms': 'displayList',
        '': 'displayList'
      },

      displayDetail: function(id) {
        vent.trigger('room:show:detail', {id: id});
      },

      displayList: function() {
        vent.trigger('room:show:list', {});
      },

      updateUrlForDetail: function(options) {
        this.navigate('rooms/'+options.id);
      },

      updateUrlForList: function(options) {
        this.navigate('rooms');
      }

    });

    return RoomListRouter;

});