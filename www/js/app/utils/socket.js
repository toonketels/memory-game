/**
 * socket
 *
 * Returns the current socket connection. If none exists, creates one.
 */
define(
  [
    'config/app-config'
  ], 
  function( AppConfig ) {

    var socket = io.connect(AppConfig.socketUrl);

    return socket;

});