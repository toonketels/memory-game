define(
  [
    'backbone.wreqr'
  ], 
  function( Wreqr ){

    return new Wreqr.Commands();

});