/**
 * AppState
 *
 * Keeps track of the application state.
 *
 * Objects can set values, get values and be notified when
 * values change.
 */
define(
  [
    'backbone'
  ],
  function( Backbone ) {
    
    'use strict';

    var AppState = Backbone.Model.extend({

    });

    return new AppState();

});