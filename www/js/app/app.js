/**
 * Our top level application.
 */
define(
  [
    'marionette',
    'apps/main/main-app',
    'apps/room/room-app',
    'apps/game/game-app',
    'utils/vent'
  ],

  function( Marionette, MainApp, RoomApp, GameApp, vent ){

    "use strict";

    var app = new Marionette.Application();

    /**
     * Add regions.
     */
    app.addRegions({
      'headerRegion': '.header',
      'mainRegion': '.main',
      'footerRegion': '.footer'
    });


    /**
     * Code run when application starts.
     */
    app.addInitializer(function() {

      /**
       * Add modules.
       */
      app.apps = {};
      app.apps.mainApp = new MainApp();
      app.apps.roomApp = new RoomApp();
      app.apps.gameApp = new GameApp();

    });

    app.on('initialize:after', function() {
      if (Backbone.history) {
        Backbone.history.start({pushState: true});
      }
    });


    app.on('close', function() {
      app.apps.roomApp.close();
      app.apps.gameApp.close();
      app.apps.mainApp.close();
    });


    vent.on('room:display:all', function(options){
      app.mainRegion.show(options.view);
    });


    return app;
});