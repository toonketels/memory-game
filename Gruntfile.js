'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jade: {
      html: {
        files: {
          'www/': ['jade/*.jade']
        },
        options: {
          client: false,
          pretty: true
        }
      }
    },
    sass: {
      dev: {
        options: {
          style: 'expanded'
        },
        files: {
          'www/css/main.css': 'sass/main.sass' 
        }
      }
    },
    shell: {
      'serve': {
        command: 'node server/app.js',
        options: {
          stdout: true
        }
      }
    },
    regarde: {
      html: {
        files: ['jade/*.jade', 'jade/includes/*.jade'],
        tasks: ['jade'],
        events: true
      },
      css: {
        files: ['sass/*.sass'],
        tasks: ['sass'],
        events: true
      }
    }
  });

  // Load contrib tasks...
  grunt.loadNpmTasks('grunt-jade');
  grunt.loadNpmTasks('grunt-regarde');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-shell');

  // Register tasks aliases...
  grunt.registerTask('build', 'Compile all jade/sass into html/css ready to be served.', ['jade', 'sass']);
  grunt.registerTask('serve', 'Starts the server.', ['shell:serve']);
  grunt.registerTask('default', 'Build and watch for changes', ['build', 'regarde']);
};